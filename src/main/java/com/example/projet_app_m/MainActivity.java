package com.example.projet_app_m;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.model.Calendar;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,Prochain_Cour.OnFragmentInteractionListener {
        public CoursDbHelper mCourseDbHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCourseDbHelper = new CoursDbHelper(this);
        List<cour> data = mCourseDbHelper.get2FirstCoursesNext();
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (savedInstanceState == null) {
            loadFragment(new Prochain_Cour());
            navigationView.setCheckedItem(R.id.prochain_cours);
        }
        /*for (Course c : mCourseDbHelper.getNextEvals()) {
            System.out.println(c);
        }*/
        new UpdateSchedule().execute("https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/diplome/2-L3IN");

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

     if (id == R.id.prochain_cours) {

    } else if (id == R.id.aujourdhui) {

    } else if (id == R.id.calendrier) {

    } else if (id == R.id.exams) {

    }

    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void loadFragment(Fragment fragment) {
// create a FragmentManager
        FragmentManager fm = getSupportFragmentManager();
// create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
// replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.frameLayout, fragment);
        fragmentTransaction.commit(); // save the changes
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    class UpdateSchedule extends AsyncTask<String, Void, List<cour>> {

        @RequiresApi(api = Build.VERSION_CODES.O)
        protected List<cour> doInBackground(String... urls) {
            List<cour> mCourses = null;
            URLConnection urlConnection;

            try {
                URL url = new URL(urls[0]);
                urlConnection = url.openConnection();
                //Log.d("exception connect :", "Update Shedule");

                urlConnection.connect();
                InputStream inputStream = urlConnection.getInputStream();
                Log.d("inputStream :", inputStream.toString());
                CalendarBuilder builder = new CalendarBuilder();
                Calendar calendar = builder.build(inputStream);
                Parser parser = new Parser(calendar);
                mCourses = parser.parseCalendar();
                mCourseDbHelper.populate(mCourses);



            } catch (Exception e) {
                Log.d("exception connect :", "Update Shedule fail");
                e.printStackTrace();
            }


            return mCourses;
        }

        @Override
        protected void onPostExecute(List<cour> courses) {

            super.onPostExecute(courses);

        }
    }
}
