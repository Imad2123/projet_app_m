package com.example.projet_app_m;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;



/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Prochain_Cour.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Prochain_Cour#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Prochain_Cour extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String LOG_TAG = "lalalalalalalalalalal" ;


    // TODO: Rename and change types of parameters
    private List<cour> mParam1;
    private CoursDbHelper mCourseDbHelper;
    private OnFragmentInteractionListener mListener;
    private TextView prof , salle , type,debut,fin,cour;
    private cour mCourse1, mCourse2;

    public Prochain_Cour() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment NextCourses.
     */
    // TODO: Rename and change types and number of parameters
    public static Prochain_Cour newInstance(List<cour> param1) {
        Prochain_Cour fragment = new Prochain_Cour();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM1, (ArrayList<? extends Parcelable>) param1);


        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getParcelableArrayList(ARG_PARAM1);
            mCourse1 = mParam1.get(0);
            mCourse2 = mParam1.get(1);


        } else {
            mCourseDbHelper = new CoursDbHelper(getContext());
            mParam1=mCourseDbHelper.get2FirstCoursesNext();
            if (mParam1 != null) {
                mParam1 = mCourseDbHelper.get2FirstCoursesNext();
                mCourse1 = mParam1.get(0);
                mCourse2 = mParam1.get(1);
            }
        }


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View myFragmentView = inflater.inflate(R.layout.prochain_cour, container, false);
        cour = myFragmentView.findViewById(R.id.cour);
        //course2 = myFragmentView.findViewById(R.id.day_course_description);
        prof = myFragmentView.findViewById(R.id.prof);
        //teacher2 = myFragmentView.findViewById(R.id.next_course_teacher2);
        salle = myFragmentView.findViewById(R.id.salle);
        //room2 = myFragmentView.findViewById(R.id.next_course_room2);
        debut = myFragmentView.findViewById(R.id.debut);
        //startDate2 = myFragmentView.findViewById(R.id.day_course_start2);
        fin = myFragmentView.findViewById(R.id.fin);
        //endDate2 = myFragmentView.findViewById(R.id.day_course_end);
        //group = myFragmentView.findViewById(R.id.next_course_group);
        //group2 = myFragmentView.findViewById(R.id.next_course_group2);
        type = myFragmentView.findViewById(R.id.type);
        //type2 = myFragmentView.findViewById(R.id.next_course_type2);



        if (mCourse1 != null && mCourse2 != null) {

            //Log.(mCourse1.getTeacher());
            cour.setText("blabla");
            prof.setText((mCourse1.getTeacher()));
            salle.setText(mCourse1.getRoom());
            debut.setText(mCourse1.getStartDate());
            fin.setText(mCourse1.getEndDate());
            //group.setText(mCourse1.getGroup());
            type.setText(mCourse1.getType());

            /**
            course2.setText(mCourse2.getName());
            teacher2.setText((mCourse2.getTeacher()));
            room2.setText(mCourse2.getRoom());
            startDate2.setText(mCourse2.getStartDate());
            endDate2.setText(mCourse2.getEndDate());
            group2.setText(mCourse2.getGroup());
            type2.setText(mCourse2.getType());
             **/
        }
        return myFragmentView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

