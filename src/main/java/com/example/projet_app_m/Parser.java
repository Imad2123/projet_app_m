package com.example.projet_app_m;

import android.os.Build;
import android.support.annotation.RequiresApi;

import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.Property;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.split;

public class Parser {

    private static final String TAG = Parser.class.getSimpleName();
    private Calendar mCalendar;
    private cour course;


    public Parser() {


    }

    public Parser(Calendar calendar) {

        mCalendar = calendar;
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public List<cour> parseCalendar() {
        List<cour> res = new ArrayList<>();
        for (final Object o : mCalendar.getComponents()) {
            Component component = (Component) o;

            if (!component.getProperty("UID").getValue().contains("Ferie"))
                if (!component.getProperty("UID").getValue().contains("ANNULE")) {
                    course = new cour();
                    for (final Object o1 : component.getProperties()) {
                        Property property = (Property) o1;
                        // System.out.println(property.getName() + ": " + property.getValue());
                        if (property.getName().equals("UID"))
                            course.setUid(property.getValue());
                        else if (property.getName().equals("LOCATION"))
                            course.setRoom(property.getValue());
                        else if (property.getName().equals("DESCRIPTION"))
                            parseDescription(property.getValue());
                        else if (property.getName().equals("DTSTART"))
                            course.setStartDate(parseDate(property.getValue()));
                        else if (property.getName().equals("DTEND"))
                            course.setEndDate(parseDate(property.getValue()));
                        else if (property.getName().equals("SUMMARY"))
                            course.setSummary(property.getValue());

                    }
                    res.add(course);
                }
        }
        return res;
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private String parseDate(String dateValue) {

        // SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
        //dateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
        // Date date = dateFormat.parse(dateValue);
        //dateFormat.applyLocalizedPattern("yyyy-MM-dd HH:mm:ss'Z'");
        //String res = dateFormat.format(date);

        LocalDateTime localDateTime = LocalDateTime.parse(dateValue, DateTimeFormatter.ofPattern("yyyyMMdd'T'HHmmss'Z'"));
        ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, ZoneId.of("Europe/Paris"));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        zonedDateTime = zonedDateTime.plusSeconds(zonedDateTime.getOffset().getTotalSeconds());
        String formattedString = zonedDateTime.format(formatter);

        return formattedString;
    }

    private void parseDescription(String description) {
        course.setDescription(description);
        String[] properties = split(description, "\n");
        for (int i = 0; i < properties.length; i++) {
            String[] property = split(properties[i], ":");
            if (property[0].contains("Matière"))
                course.setName(property[1].substring(1));
            else if (property[0].contains("Enseignant"))
                course.setTeacher(property[1].substring(1));
            else if (property[0].contains("Promotion") || property[0].contains("Promotions"))
                course.setPromotion(property[1].substring(1));
            else if (property[0].contains("TD"))
                course.setGroup(property[1].substring(1));
            else if (property[0].contains("Type"))
                course.setType(property[1].substring(1));
        }
    }

    public Calendar getCalendar() {
        return mCalendar;
    }

    public void setCalendar(Calendar calendar) {
        mCalendar = calendar;
    }


}

