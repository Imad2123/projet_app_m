package com.example.projet_app_m;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_REPLACE;

public class CoursDbHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "courses.db";
    public static final String TABLE_NAME = "courses";
    public static final String _ID = "_id";
    public static final String COLUMN_UID = "uid";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_START_DATE = "startDate";
    public static final String COLUMN_END_DATE = "endDate";
    public static final String COLUMN_TEACHER = "teacher";
    public static final String COLUMN_ROOM = "room";
    public static final String COLUMN_PROMOTION = "promotion";
    public static final String COLUMN_GROUP = "groupe";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_SUMMARY = "summary";
    public static final String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";
    public static final String SQL_CREATE_COURSES_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
            _ID + " INTEGER PRIMARY KEY," +

            COLUMN_UID + " TEXT NOT NULL, " +
            COLUMN_NAME + " TEXT NOT NULL, " +
            COLUMN_START_DATE + " TEXT NOT NULL, " +
            COLUMN_END_DATE + " TEXT NOT NULL, " +
            COLUMN_TEACHER + " TEXT, " +
            COLUMN_ROOM + " TEXT , " +
            COLUMN_PROMOTION + " TEXT , " +
            COLUMN_GROUP + " TEXT , " +
            COLUMN_TYPE + " TEXT , " +
            COLUMN_DESCRIPTION + " TEXT , " +
            COLUMN_SUMMARY + " TEXT , " +
            // To assure the application have just one same course entry
            " UNIQUE (" + COLUMN_UID + ") ON CONFLICT REPLACE);";
    private static final String TAG = CoursDbHelper.class.getSimpleName();
    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public CoursDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(SQL_CREATE_COURSES_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(TABLE_DROP);
        onCreate(db);
    }


    /**
     * Adds a new course
     *
     * @return true if the course was added to the table ; false otherwise
     */
    public boolean addCourse(cour course) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_UID, course.getUid());
        values.put(COLUMN_NAME, course.getName());
        values.put(COLUMN_START_DATE, course.getStartDate());
        values.put(COLUMN_END_DATE, course.getEndDate());
        values.put(COLUMN_TEACHER, course.getTeacher());
        values.put(COLUMN_ROOM, course.getRoom());
        values.put(COLUMN_PROMOTION, course.getPromotion());
        values.put(COLUMN_GROUP, course.getGroup());
        values.put(COLUMN_TYPE, course.getType());
        values.put(COLUMN_DESCRIPTION, course.getDescription());
        values.put(COLUMN_SUMMARY, course.getSummary());


        Log.d(TAG, "adding: " + course.getName() + " with id=" + course.getId());

        // Inserting Row
        // The unique used for creating table ensures to have only one copy of each pair
        // If rowID = -1, an error occured
        long rowID = db.insertWithOnConflict(TABLE_NAME, null, values, CONFLICT_REPLACE);
        db.close(); // Closing database connection

        return (rowID != -1);
    }


    /**
     * Returns a cursor on all the cities of the data base
     */
    public Cursor fetchAllCourses() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null,
                null, null, null, null, "datetime(" + COLUMN_START_DATE + ") ASC", null);

        Log.d(TAG, "call fetchAllCourses()");
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public List<cour> get2FirstCoursesNext() {
        List<cour> res = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_NAME + " where datetime(" + COLUMN_START_DATE
                + ") > datetime('now') ORDER BY datetime(" + COLUMN_START_DATE + ") ASC Limit 2", null);
        while (cursor.moveToNext()) {
            res.add(cursorToCourse(cursor));
        }
        db.close();
        return res;
    }

    public List<cour> getCoursesOfDay() {
        List<cour> res = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_NAME + " where date(" + COLUMN_START_DATE
                + ") = date('now') ORDER BY datetime(" + COLUMN_START_DATE + ") ASC", null);
        while (cursor.moveToNext()) {
            res.add(cursorToCourse(cursor));
        }
        db.close();
        return res;
    }

    public List<cour> getNextEvals() {
        List<cour> res = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_NAME + " where date(" + COLUMN_START_DATE
                + ") >= date('now') and " + COLUMN_TYPE + " = 'Evaluation' ORDER BY datetime(" + COLUMN_START_DATE + ") ASC", null);
        while (cursor.moveToNext()) {
            res.add(cursorToCourse(cursor));
        }
        db.close();
        return res;
    }

    public List<cour> getCoursesOfDay(String date) {
        List<cour> res = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_NAME + " where date(" + COLUMN_START_DATE
                + ") = date(?) ORDER BY datetime(" + COLUMN_START_DATE + ") ASC", new String[]{date});
        while (cursor.moveToNext()) {
            res.add(cursorToCourse(cursor));
        }
        db.close();
        return res;
    }

    /**
     * Returns a list on all the courses of the data base
     */
    public List<cour> getAllCourses() {
        List<cour> res = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = fetchAllCourses();
        while (cursor.moveToNext()) {
            res.add(cursorToCourse(cursor));
        }
        db.close();
        return res;
    }


    public cour getCourse(int id) {
        cour course;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_NAME + " where id = ?", new String[]{String.valueOf(id)});
        course = cursorToCourse(cursor);
        db.close();
        return course;
    }


    public void deleteAllCourses() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(TABLE_DROP);
        onCreate(db);
        db.close();
    }

    public void populate(List<cour> courses) {
        if (courses != null && !courses.isEmpty()) {
            deleteAllCourses();
            Log.d(TAG, "call populate()");
            for (cour c : courses) {
                addCourse(c);
            }

            SQLiteDatabase db = this.getReadableDatabase();
            long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM " + TABLE_NAME, null);
            Log.d(TAG, "nb of rows=" + numRows);
            db.close();
        }
    }


    public cour cursorToCourse(Cursor cursor) {
        cour course = new cour(cursor.getLong(cursor.getColumnIndex(_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_UID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_NAME)),
                cursor.getString(cursor.getColumnIndex(COLUMN_START_DATE)),
                cursor.getString(cursor.getColumnIndex(COLUMN_END_DATE)),
                cursor.getString(cursor.getColumnIndex(COLUMN_TEACHER)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ROOM)),
                cursor.getString(cursor.getColumnIndex(COLUMN_PROMOTION)),
                cursor.getString(cursor.getColumnIndex(COLUMN_GROUP)),
                cursor.getString(cursor.getColumnIndex(COLUMN_TYPE)),
                cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)),
                cursor.getString(cursor.getColumnIndex(COLUMN_SUMMARY))
        );
        return course;
    }


}

