package com.example.projet_app_m;


import android.os.Parcel;
import android.os.Parcelable;

class cour implements Parcelable {

    public static final String TAG = cour.class.getSimpleName();
    public static final Creator<cour> CREATOR = new Creator<cour>() {
        @Override
        public cour createFromParcel(Parcel in) {
            return new cour(in);
        }

        @Override
        public cour[] newArray(int size) {
            return new cour[size];
        }
    };
    private long id;
    private String uid;
    private String name;
    private String startDate;
    private String endDate;
    private String teacher;
    private String room;
    private String promotion;
    private String group;
    private String type;
    private String description;
    private String summary;

    public cour() {
    }

    public cour(String uid, String name, String startDate, String endDate,
                  String teacher, String room, String promotion, String group, String type, String description, String summary) {
        this.uid = uid;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.teacher = teacher;
        this.room = room;
        this.promotion = promotion;
        this.group = group;
        this.type = type;
        this.description = description;
        this.summary = summary;
    }

    public cour(long id, String uid, String name, String startDate, String endDate,
                  String teacher, String room, String promotion, String group, String type, String description, String summary) {
        this.id = id;
        this.uid = uid;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.teacher = teacher;
        this.room = room;
        this.promotion = promotion;
        this.group = group;
        this.type = type;
        this.description = description;
        this.summary = summary;
    }

    public cour(Parcel in) {
        id = in.readLong();
        uid = in.readString();
        name = in.readString();
        startDate = in.readString();
        endDate = in.readString();
        teacher = in.readString();
        room = in.readString();
        promotion = in.readString();
        group = in.readString();
        type = in.readString();
        description = in.readString();
        summary = in.readString();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPromotion() {
        return promotion;
    }

    public void setPromotion(String promotion) {
        this.promotion = promotion;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", uid='" + uid + '\'' +
                ", name='" + name + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", teacher='" + teacher + '\'' +
                ", room='" + room + '\'' +
                ", promotion='" + promotion + '\'' +
                ", group='" + group + '\'' +
                ", type='" + type + '\'' +
                ", description='" + description + '\'' +
                ", summary='" + summary + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(uid);
        dest.writeString(name);
        dest.writeString(startDate);
        dest.writeString(endDate);
        dest.writeString(teacher);
        dest.writeString(room);
        dest.writeString(promotion);
        dest.writeString(group);
        dest.writeString(type);
        dest.writeString(description);
        dest.writeString(summary);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}

